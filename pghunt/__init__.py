from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from celery import Celery
from elasticsearch import Elasticsearch


app = Flask(__name__)
app.config['SECRET_KEY'] = '6791628bb0b13ce0c676dfde280va245'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
app.config['ELASTICSEARCH_URL'] = '10.10.4.178:9200'

# celery config
app.config['CELERY_BROKER_URL'] = 'amqp://localhost//'
app.config['CELERY_RESULT_BACKEND'] = 'amqp://localhost//'
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'], backend=app.config['CELERY_RESULT_BACKEND'])
celery.conf.update(app.config)

# elasticsearch cofig
if app.config['ELASTICSEARCH_URL']:
    app.elasticsearch = Elasticsearch([app.config['ELASTICSEARCH_URL']])
else:
    app.elasticsearch = None


db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'info'

from pghunt import routes
