import os
from pghunt.services.download_posts import download_file
from pghunt.models import User, Post, Bookings
from pghunt import app, db, bcrypt
from flask import render_template, url_for, flash, redirect, request, abort, jsonify, send_file
from pghunt.forms import RegistrationForm, LoginForm, PostForm
from flask_login import login_user, current_user, logout_user, login_required
import csv
from pghunt import celery


@app.route("/")
@app.route("/home")
def home():
    posts = Post.query.all()
    return render_template('home.html', posts=posts)


@app.route("/about")
def about():
    return render_template('about.html', title="About")


@app.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.username.data, email=form.email.data, password=hashed_password, account_type=form.account_type.data)
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created! You are now able to log in', 'success')
        return redirect(url_for('home'))
    return render_template('register.html', title="Register", form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            # next_page = request.args.get('next')
            # return redirect(next_page) if next_page else redirect(url_for('home'))
            return redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check email and password', 'danger')
    return render_template('login.html', title="Login", form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home'))


def save_picture(form_picture):

    return


@app.route("/posts/create-post/", methods=['GET', 'POST'])
@login_required
def new_post():
    print(request.method)
    if current_user.account_type == 'owner':
        form = PostForm()
        if form.validate_on_submit():
            print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=")
            if form.pg_picture.data:
                pg_picture_file = save_picture(form.pg_picture.data)
                Post.pg_pic = pg_picture_file
            pg_pic = url_for('static', filename='profile_pics/' + Post.pg_pic)
            post = Post(title=form.title.data, details=form.details.data, price=form.price.data, contact=form.contact.data, address=form.address.data, pg_pic=pg_pic, user=current_user)
            db.session.add(post)
            db.session.commit()
            flash('You post created', 'success')
            return redirect(url_for('home'))
        return render_template('create_post.html', title='New Post', form=form, legend='New Post')
    else:
        flash('Login as Owner', 'danger')
        return redirect(url_for('home'))


@app.route("/posts/<int:post_id>")
def post(post_id):
    post = Post.query.get_or_404(post_id)
    return render_template('post.html', title=post.title, post=post)


@app.route("/posts/")
def post_lists():
    if current_user.account_type == 'owner':
        posts = Post.query.filter_by(user=current_user)
        return render_template('post-list.html', title="Posts", posts=posts)
    else:
        flash('Login as Owner', 'danger')
        return redirect(url_for('home'))


@app.route("/book/<int:post_id>")
def book(post_id):
    owner = Post.query.get_or_404(post_id)
    book = Bookings(post_id=post_id, customer_id=current_user.id, owner=owner.user_id)
    db.session.add(book)
    db.session.commit()
    return redirect(url_for('home'))


@app.route("/bookings/")
def bookings():
    bookings = Bookings.query.filter_by(owner=current_user.id)
    return render_template('bookings.html', title='booking', bookings=bookings)


@app.route("/delete/<int:post_id>", methods=['POST'])
def delete(post_id):
    post = Post.query.get_or_404(post_id)
    if post.user == current_user:
        db.session.delete(post)
        db.session.commit()
        flash('Your Post has been Deleted', 'success')
        return redirect(url_for('post_lists'))
    else:
        flash('Not your post', 'danger')
    return redirect(url_for('home'))


@app.route("/update/<int:post_id>", methods=['GET','POST'])
def update(post_id):
    form = PostForm()
    post = Post.query.get_or_404(post_id)
    if form.validate_on_submit():
        post.title = form.title.data
        post.details = form.details.data
        post.price = form.price.data
        post.contact = form.contact.data
        post.address = form.address.data
        db.session.commit()
        flash('Updated', 'success')
        return redirect(url_for('post', post_id=post.id))
    elif request.method == 'GET':
        form.title.data = post.title
        form.details.data = post.details
        form.price.data = post.price
        form.contact.data = post.contact
        form.address.data = post.address
    return render_template('create_post.html', title='Update', form=form)


@app.route("/download-pg-lists/<int:user_id>", methods=['GET'])
def download_pg_lists(user_id):
    if user_id == current_user.id:
        task_id = download_file.delay(user_id, current_user.username)
        print("{0} Task after doenload".format(task_id))

        return render_template('download.html', id=task_id)
        # return redirect(url_for('home'))


@app.route('/status')
def taskstatus():
    id = request.args.get('id')
    task = download_file.AsyncResult(id)
    print("{0} Task at the taskstatus".format(task.state))

    return jsonify({'status': task.ready()})

@app.route('/download')
def downloadFile():
    #For windows you need to use drive name [ex: F:/Example.pdf]
    path = "/home/nike/"+str(current_user.username) + ".csv"
    return send_file(path, as_attachment=True)



@app.route("/search/")
def search():
    q = request.args.get('q')
    posts, total = Post.search(q, 1, 5)

    if total:
        flash(str(total) + " search found", 'success')
        return render_template('post-list.html', title="Posts", posts=posts)
    else:
        flash("No search found", 'warning')
        return redirect(url_for('home'))
