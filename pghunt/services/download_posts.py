from pghunt import celery
from pghunt.models import Post
import csv
import os

@celery.task(name="download_posts.download_file")
def download_file(user_id, name):
    posts = Post.query.filter_by(user_id=user_id)
    all_posts = []
    for post in posts:
        post_list = []
        post_list.append(post.title)
        post_list.append(post.date_posted)
        post_list.append(post.details)
        post_list.append(post.price)
        post_list.append(post.contact)
        post_list.append(post.address)
        all_posts.append(post_list)



    fields = ['Title', 'Date', 'Details', 'Price', 'Contact', 'Address']
    with open(os.path.join('/home/'+os.environ.get('USER'), name+".csv"), 'w') as csv_file:
        csvwriter = csv.writer(csv_file)
        csvwriter.writerow(fields)
        csvwriter.writerows(all_posts)

    return "Done"
